# whypr

Configuration files needed for Hyprland, kitty, nvim, ranger, shell, swayidle, swayimg, swaylock, waybar, wofi, zsh, bash.

Some of the scripts needed to automate system tasks with Hyprland, working with wofi.
